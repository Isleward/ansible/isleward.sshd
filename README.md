# Ansible Role: sshd

* Changes ssh daemon port
* Opens the new ssh port in firewalld
* Optionally configures SELinux to acknowledge the new port
* Disables ssh login for the root account
* Disables password authentication

## Requirements

  CentOS 7

## Role Variables

Different ssh port

```yml
desired_ssh_port: 2599
```

To enable SELinux configuration

```yml
setup_seport: true
```

## Dependencies

  None.

## Example Playbook

```yml
- hosts: localhost
  roles:
    - isleward.sshd
```

## Author Information

This role was created in 2018 by Vildravn.